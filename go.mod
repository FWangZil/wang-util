module gitlab.com/FWangZil/wang-util

go 1.14

require (
	github.com/FWangZil/errkit v0.9.0
	github.com/cespare/xxhash v1.1.0 // indirect
	github.com/coocood/freecache v1.1.0
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/disintegration/imaging v1.6.2
	github.com/golang/freetype v0.0.0-20170609003504-e2365dfdc4a0
	github.com/gotit/go-net v0.0.0-20180409072212-f5db4f3f7f30
	github.com/meikeland/errkit v0.9.0
	github.com/pkg/errors v0.9.1
	github.com/satori/go.uuid v1.2.0
	github.com/shopspring/decimal v0.0.0-20200226005030-a08b92da27ed
	github.com/stretchr/testify v1.5.1
	golang.org/x/crypto v0.0.0-20200221231518-2aa609cf4a9d
	golang.org/x/image v0.0.0-20200119044424-58c23975cae1
)
