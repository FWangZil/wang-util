package localNet

import (
	"strings"

	"github.com/gotit/go-net/net"
	"github.com/meikeland/errkit"
)

const (
	respOk      = "OK"
	respFail    = "FAIL"
	respUnLogin = "UNLOGIN"
)

var (
	// Host 服务地址
	Host string
	// Client 服务的客户端
	Client *ApiClient
)

// API api
type ApiClient struct {
	Net    *net.Net
	Header map[string]string
}

func NetError(err error) error {
	message := err.Error()
	message = strings.Replace(message, "\"", "", -1)
	return errkit.Errorf(message)
}
