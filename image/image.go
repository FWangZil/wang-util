package image

import (
	"flag"
	"fmt"
	"image"
	"image/color"
	"io/ioutil"
	"log"
	"time"
	"wang-util/tools"

	"github.com/disintegration/imaging"
	"github.com/golang/freetype"
	"github.com/golang/freetype/truetype"
	"golang.org/x/image/font"
	"golang.org/x/image/math/fixed"
)

// 在图片上拼接文字
var dpi = flag.Float64("dpi", 2048, "screen resolution")

// CompositePicture 合成知产图片
func CompositePicture(code string) (string, string, error) {
	beginTime := time.Now()

	// 获取模板的图片
	template, err := imaging.Open("./web/static/temp/poster2bg.png")
	if err != nil {
		log.Println("打开第二张图片处理出错")
		log.Println(err)
		return "", "", err
	}

	// 开始写入文字
	templateSource := imaging.Clone(template)
	err = writeTextToImage(templateSource, code)
	if err != nil {
		log.Printf("%+#v", err)
	}

	if err != nil {
		log.Println("添加图片错")
		log.Println(err)
		return "", "", err
	}
	// 将图片存到本地（可删除）
	newUUID := fmt.Sprintf("%s.png", tools.NewUUID())
	objKey := fmt.Sprintf("./web/static/poster/%s", newUUID)
	fileName := fmt.Sprintf(objKey)
	err = imaging.Save(templateSource, fileName)
	if err != nil {
		log.Println(err)
	}

	//// 将图片存入OSS
	//buf := new(bytes.Buffer)
	//err = jpeg.Encode(buf, templateSource, nil)
	//if err != nil {
	//	log.Println(err)
	//	return "", "", err
	//}
	//
	//send := buf.Bytes()
	//reader := bytes.NewReader(send)
	//ossConfig := conf.GetStaticOSS()
	//if err := aliyunoss.WriteStaticToOSS(reader, objKey); err != nil {
	//	log.Println("上传下载二维码图片失败:", err)
	//	return "", "", err
	//}
	//
	//picURL := fmt.Sprintf("%s/%s", ossConfig.BindHost, objKey)
	//
	fmt.Println("CompositePicture elapsed: ", time.Since(beginTime))

	return "picURL", newUUID, err
}

// writeTextToImage 写入上链证书文本
func writeTextToImage(target *image.NRGBA, code string) error {
	beginTime := time.Now()
	c := freetype.NewContext()

	c.SetDPI(*dpi)
	c.SetClip(target.Bounds())
	c.SetDst(target)
	c.SetHinting(font.HintingFull)
	c.SetSrc(image.NewUniform(color.RGBA{R: 91, G: 96, B: 255, A: 255}))
	c.SetFontSize(1.8)

	fontFam, err := getFontFamily()
	if err != nil {
		fmt.Println("get font family error")
	}
	c.SetFont(fontFam)
	var pt fixed.Point26_6

	pt = freetype.Pt(500, 1025)
	_, err = c.DrawString(code, pt)
	if err != nil {
		fmt.Printf("draw error: %v \n", err)
	}
	fmt.Println("writeTextToImage elapsed: ", time.Since(beginTime))

	fmt.Println("writeAssetOnImageAll elapsed: ", time.Since(beginTime))
	return nil
}

// 获取字体文件
func getFontFamily() (*truetype.Font, error) {
	// 这里需要读取中文字体，否则中文文字会变成方格
	//objKey := fmt.Sprintf("upload/%s", "Alibaba-PuHuiTi-Medium.ttf")
	//respFont, err := aliyunoss.ReadStaticFromOSS(objKey)
	//if err != nil {
	//	return nil, err
	//}

	fontBytes, err := ioutil.ReadFile("./web/static/Alibaba-PuHuiTi-Medium.ttf")
	if err != nil {
		fmt.Println("read file error:", err)
		return &truetype.Font{}, err
	}

	f, err := freetype.ParseFont(fontBytes)
	if err != nil {
		fmt.Println("parse font error:", err)
		return &truetype.Font{}, err
	}

	return f, err
}
