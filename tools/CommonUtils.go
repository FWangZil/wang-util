package tools

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"net/http"
	"strconv"
)

/**
val is string type
*/
func IsString(val interface{}) bool {
	_, ok := val.(string)
	return ok
}

/**
value is int type
*/
func IsInt(value interface{}) bool {
	_, ok := value.(int)
	return ok
}
func IsInt64(value interface{}) bool {
	_, ok := value.(int64)
	return ok
}

func IsFloat64(value interface{}) bool {
	_, ok := value.(float64)
	return ok
}

/**
* value is Map[string]interface{}
**/
func IsMap(value interface{}) bool {
	_, ok := value.(map[string]interface{})
	return ok
}

/**
* value convert map[string]interface{}
**/
func ConvertMap(value interface{}) (map[string]interface{}, error) {

	mapVal, ok := value.(map[string]interface{})
	if ok {
		return mapVal, nil
	} else {
		fmt.Println("无法将", value, "转换到Map")
		return nil, errors.New("无法将value转换到Map[string]interface{}")
	}

}

func ParserUint(value interface{}) (uint, error) {
	num, ok := value.(float64)
	if ok {
		return uint(num), nil
	} else {
		return 0, errors.New("无法将Value转换成int")
	}
}
func ParserString(value interface{}) (string, error) {
	str, ok := value.(string)
	if ok {
		return str, nil
	} else {
		return "", errors.New("无法将Value转换成string")
	}
}

/**
 * 获取Cookie 信息
 * name ->cookie.name
 * defVal 默认值
 */
func GetCookie(req *http.Request, name, defVal string) string {
	for _, cookie := range req.Cookies() {
		if cookie.Name == name {
			return cookie.Value
		}
	}
	return defVal
}

func IntAsString(num int) string {
	return strconv.FormatInt(int64(num), 10)
}

func Int64AsString(num int64) string {
	return strconv.FormatInt(num, 10)
}

func ReadAsBuffer(r io.Reader) *bytes.Buffer {
	body := &bytes.Buffer{}
	_, err := body.ReadFrom(r)
	Assert(err)
	return body
}
