package tools

import "testing"

//单元测试
func TestIsString(t *testing.T) {
	if !IsString("xx") {
		t.Error("faild")
	}
	if IsString(nil) {
		t.Error("faild")
	}
	if IsString(true) {
		t.Error("faild")
	}
}

func TestIsInt(t *testing.T) {
	if !IsInt(10000000) {
		t.Error("faild")
	}
	if IsInt(10000000.0) {
		t.Error("faild")
	}
	var num interface{} = 10
	if !IsInt(num) {
		t.Error("faild")
	}
}

//go test mycalc
