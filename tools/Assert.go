package tools

import (
	"log"
)

func Assert(err error) {
	if err != nil {
		log.Println("[ERROR]")
		log.Fatal(err)
		panic(err)

	}
}
